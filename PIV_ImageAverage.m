function [velVector] = PIV_ImageAverage(filePattern, fDir, argstr)

    addpath(fDir)
    
    baseStr = filePattern;

    velVector = [];

    for i = 1:50
        num1 = i;
        num2 = i + 1;

        if num1 > 9
            str1 = strcat(['00', num2str(num1)]);
        else
            str1 = strcat(['000', num2str(num1)]);
        end % end if

        if num2 > 9
            str2 = strcat(['00', num2str(num2)]);
        else
            str2 = strcat(['000', num2str(num2)]);
        end % end if

        fileStr = strcat([str1 '-' str2 baseStr]);

        %% load data
        fileData = importdata(fileStr);
        
        if isstruct(fileData)
            fileVectors = fileData.data;
        else
            fileVectors =  fileData;
        end

        r = length(fileVectors(1, :));
        
        if i == 1
            velVector = fileVectors;
            averageVec = ones(length(fileVectors), 1);            
        else

           %velVector = [velVector ;fileVectors];
           for j = 1:length(fileVectors)
               iVec = find(velVector(:, 2) == fileVectors(j, 2) & velVector(:, 1) == fileVectors(j, 1));
               if iVec % true if coordinate in the array
                   velVector(iVec, 3:r) = (velVector(iVec, 3:r) + fileVectors(j, 3:r));
                   averageVec(iVec) = averageVec(iVec) + 1;
               else % adds coordinate to the array
                   velVector = [velVector; fileVectors(j, :)];
                   averageVec = [averageVec; 1];
               end % end if
           end % end for
           %velVector(:, 3:4) = velVector(:, 3:4) + fileVectors(:, 3:4); 
        end % end if
    end % end for
 
    for i = 3:(r-2)
        velVector(:, i) = velVector(:, i) ./ averageVec;
    %velVector(:, 4) = velVector(:, 4) ./ averageVec;
    end
    
    if strcmp(argstr, 'average')
        % do nothing
    elseif strcmp(argstr, 'std')
        averageVector = velVector;
        for i = 1:50
            num1 = i;
            num2 = i + 1;

            if num1 > 9
                str1 = strcat(['00', num2str(num1)]);
            else
                str1 = strcat(['000', num2str(num1)]);
            end % end if

            if num2 > 9
                str2 = strcat(['00', num2str(num2)]);
            else
                str2 = strcat(['000', num2str(num2)]);
            end % end if

            fileStr = strcat([str1 '-' str2 baseStr]);

            %% load data
            fileData = importdata(fileStr);

            % piv files load as struct files
            if isstruct(fileData)
                fileVectors = fileData.data;
            else
                fileVectors =  fileData;
            end

            r = length(fileVectors(1, :));

            %velVector = [velVector ;fileVectors];
           for j = 1:length(fileVectors)
               % get coordinate of vector
               iVec = find(averageVector(:, 2) == fileVectors(j, 2) & averageVector(:, 1) == fileVectors(j, 1));
               stdVector(iVec, 3:r) = (averageVector(iVec, 3:r) - fileVectors(j, 3:r)).^2;
               %averageVec(iVec) = averageVec(iVec) + 1;
               
           %velVector(:, 3:4) = velVector(:, 3:4) + fileVectors(:, 3:4); 
            
            end % end for
            
            stdVector = stdVector ./ averageVec;
        end
    end
end % end PIV_ImageAverage