function [adv] = computeAdvection(U, X)

    xMin = min(X(:,1));
    xMax = max(X(:,1));
    yMin = min(X(:,2));
    yMax = max(X(:,2));
    nX = length(unique(X(:,1)));
    nY = length(unique(X(:,2)));
    dX = (xMax - xMin) / nX;
    dY = (yMax - yMin) / nY;
    
    [xMesh, yMesh] = meshgrid(linspace(xMin, xMax, nX), linspace(yMin, yMax, nY));
    
    U_scat = scatteredInterpolant(X(:, 1), X(:, 2), U(:, 1));
    V_scat = scatteredInterpolant(X(:, 1), X(:, 2), U(:, 1));
    
    for i = 2:nX-1
        for j = 2:nY-1
%             adv.UX = U(:, 1) .* U(:, 1) ./ X(:, 1);
            adv.UX((nY-2)*(i-2)+j-1, 1) = U_scat(xMesh(j, i), yMesh(j, i)) .* center_diff(xMesh(j,i-1:i+1)', yMesh(j, i), U_scat);
%             adv.VX = U(:, 1) .* U(:, 2) ./ X(:, 1);
            adv.UY((nY-2)*(i-2)+j-1, 1) = U_scat(xMesh(j, i), yMesh(j, i)) .* center_diff(xMesh(j,i-1:i+1)', yMesh(j, i), V_scat);
            adv.X((nY-2)*(i-2)+j-1, 1) = xMesh(j, i);
            adv.X((nY-2)*(i-2)+j-1, 2) = yMesh(j, i);
            adv.U((nY-2)*(i-2)+j-1, 1) = U_scat(xMesh(j, i), yMesh(j, i));
            adv.U((nY-2)*(i-2)+j-1, 2) = V_scat(xMesh(j, i), yMesh(j, i));
        end
    end
    
    for i = 2:nX-1
        for j = 2:nY-1
            %adv.UY = U(:, 2) .* U(:, 1) ./ X(:, 2);
            adv.VX((nY-2)*(i-2) + j-1, 1) = V_scat(xMesh(j, i), yMesh(j, i)) .* center_diff(yMesh(j-1:j+1, i), xMesh(j, i), U_scat);
            %adv.VY = U(:, 2) .* U(:, 2) ./ X(:, 2);
            adv.VY((nY-2)*(i-2) + j-1, 1) = V_scat(xMesh(j, i), yMesh(j, i)) .* center_diff(yMesh(j-1:j+1, i), xMesh(j, i), V_scat);
        end
    end
end
