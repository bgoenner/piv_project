
% data processing options
% do these first
calcData = 0;
calcAvgImage = 1;
calcSTDImage = 1;

% ploting options
calcVel = 1;
calcAdv = 1;
calcSTD = 1;
calcSNR = 1;

px2mm = @(px) 32 .* px ./ 580;
px2cm = @(px) 3.2 .* px ./ 580;

close all

global c_mat
c_mat = load('foil_mat.mat');
c_mat = px2mm(c_mat.c_mat);

if calcData

addpath('.\..\');

DataFolder = '.\PIV_Data\';
Data1 = 'GPM1-DT005030\';
Data2 = 'GPM1_DT001030\';
Data3 = 'GPM0-4_DT005030\';

post = 'piv\3_postprocess_output';
piv  = 'piv\2_piv_output';
raw  = 'raw';

postVelVectors = '_velocity_vec.txt';

%addpath('.\..\PIV_Data\GPM1-DT005030\piv\3_postprocess_output')
%addpath('.\..\PIV_Data\GPM1-DT005030\raw')

DataPost1 = strcat([DataFolder, Data1, post]);
DataPost2 = strcat([DataFolder, Data2, post]);
DataPost3 = strcat([DataFolder, Data3, post]);

DataPiv1 = strcat([DataFolder, Data1, piv]);
DataPiv2 = strcat([DataFolder, Data2, piv]);
DataPiv3 = strcat([DataFolder, Data3, piv]);

DataRaw1 = strcat([DataFolder, Data1, raw]);
DataRaw2 = strcat([DataFolder, Data2, raw]);
DataRaw3 = strcat([DataFolder, Data3, raw]);

addpath(DataPost1)
addpath(DataRaw1)
addpath(DataPost2)
addpath(DataRaw2)
addpath(DataPost3)
addpath(DataRaw3)

x_scale = 21.5;
y_scale = 45;
x_off   = 0;
y_off   = -30;
y_px = 580;
x_px = 780;




%% Velocity Vectors
% % velVect = PIV_ImageAverage('_velocity_vec.txt', DataPost1);
% % velVect(:, 3:4) = velVect(:, 3:4) * -1;
% % plot_PIV(velVect, 580, 780, 2, 17, 43.2, 42.2, [], strcat([DataRaw1 '\frame0000.jpg']), 'quiver');
% % 
% % velVect = PIV_ImageAverage('_velocity_vec.txt', DataPost2);
% % velVect(:, 3:4) = velVect(:, 3:4) * -1;
% % plot_PIV(velVect, 580, 780, 2, 17, 43.2, 42.2, [], strcat([DataRaw2 '\frame0000.jpg']), 'quiver');
% % 
% % velVect = PIV_ImageAverage('_velocity_vec.txt', DataPost3);
% % velVect(:, 3:4) = velVect(:, 3:4) * -1;
% % plot_PIV(velVect, 580, 780, 2, 17, 43.2, 42.2, [], strcat([DataRaw3 '\frame0000.jpg']), 'quiver');


%% SNR Contours
%velVect = PIV_ImageAverage('_snr.txt', DataPost3);
%plot_PIV(velVect, 580, 780, 2, 17, 43.2, 42.2, [], strcat([DataRaw3 '\frame0000.jpg']), 'contour');

%% ------------------------------------------------------------------------
% Using .piv files

% function signatures
%[velVector] = PIV_ImageAverage(filePattern, fDir)
%function [] = plot_PIV(velVector, x_px, y_px, x_off, y_off, x_scale_coeff, 
%   y_scale_coeff, removal_points, image, plotType)
addpath(DataPiv1)
addpath(DataPiv2)
addpath(DataPiv3)



%.piv files structure [x(px) y(px) dx(px) dy(px) snr peak])
if calcAvgImage
velVect1 = PIV_ImageAverage('.piv', DataPiv1, 'average');
velVect1(:, 1:4) = px2mm(velVect1(:, 1:4));

velVect2 = PIV_ImageAverage('.piv', DataPiv2, 'average');
velVect2(:,1:4) = px2mm(velVect2(:, 1:4));

velVect3 = PIV_ImageAverage('.piv', DataPiv3, 'average');
velVect3(:, 1:4) = px2mm(velVect3(:, 1:4)); 

end

if calcSTDImage

velVectSTD1 = PIV_ImageAverage('.piv', DataPiv1, 'std');
velVectSTD2 = PIV_ImageAverage('.piv', DataPiv2, 'std');
velVectSTD3 = PIV_ImageAverage('.piv', DataPiv3, 'std');

velVectSTD1(:, 1:4) = px2mm(velVectSTD1(:, 1:4)); 
velVectSTD2(:, 1:4) = px2mm(velVectSTD2(:, 1:4)); 
velVectSTD3(:, 1:4) = px2mm(velVectSTD3(:, 1:4)); 
end
end
%% compute the flow rate of the PIV data

Plot1_dx_inter = scatteredInterpolant(velVect1(:, 1), velVect1(:, 2), velVect1(:, 3));
% vel1_dx = plot_PIV([velVect1(:, 1), velVect1(:, 2), velVect1(:, 3)], 'contourf');
% plotname = catstr([name '_DX']);
% plottitle = catstr([title ' DX']);
% title(plottitle)
% savefig(plotname)
% saveas(vel1_dx, strcat([plotname '.jpg']))
Plot1_dy_inter = scatteredInterpolant(velVect1(:, 1), velVect1(:, 2), velVect1(:, 4));
% vel1_dy = plot_PIV([velVect1(:, 1), velVect1(:, 2), velVect1(:, 4)], 'contourf');
% plotname = catstr([name '_DY']);
% plottitle = catstr([title ' DY']);
% title(plottitle)
% savefig(plotname)
% saveas(vel1_dx, strcat([plotname '.jpg']))
Plot1_davg_inter = scatteredInterpolant(velVect1(:, 1), velVect1(:, 2), sqrt(velVect1(:, 4).^2 + velVect1(:, 3).^2));
% vel1_avg = plot_PIV([velVect1(:, 1), velVect1(:, 2), sqrt(velVect1(:, 4).^2 + velVect1(:, 3).^2)], 'contourf');
% plotname = catstr([name '_AVG']);
% plottitle = catstr([title ' AVG']);
% title(plottitle)
% savefig(plotname)
% saveas(vel1_avg, strcat([plotname '.jpg']))
% 
Plot2_dx_inter = scatteredInterpolant(velVect2(:, 1), velVect2(:, 2), velVect2(:, 3));
% vel2_dx = plot_PIV([velVect1(:, 1), velVect1(:, 2), velVect1(:, 3)], 'contourf');
Plot2_dy_inter = scatteredInterpolant(velVect2(:, 1), velVect2(:, 2), velVect2(:, 4));
% vel2_dy = plot_PIV([velVect1(:, 1), velVect1(:, 2), velVect1(:, 4)], 'contourf');
Plot2_davg_inter = scatteredInterpolant(velVect2(:, 1), velVect2(:, 2), sqrt(velVect2(:, 4).^2 + velVect2(:, 3).^2));
% vel2_avg = plot_PIV([velVect1(:, 1), velVect1(:, 2), sqrt(velVect1(:, 4).^2 + velVect1(:, 3).^2)], 'contourf');
% 
Plot3_dx_inter = scatteredInterpolant(velVect3(:, 1), velVect3(:, 2), velVect3(:, 3));
% vel3_dx = plot_PIV([velVect1(:, 1), velVect1(:, 2), velVect1(:, 3)], 'contourf');
Plot3_dy_inter = scatteredInterpolant(velVect3(:, 1), velVect3(:, 2), velVect3(:, 4));
% vel3_dy = plot_PIV([velVect1(:, 1), velVect1(:, 2), velVect1(:, 4)], 'contourf');
Plot3_davg_inter = scatteredInterpolant(velVect3(:, 1), velVect3(:, 2), sqrt(velVect3(:, 4).^2 + velVect3(:, 3).^2));
% vel3_avg = plot_PIV([velVect1(:, 1), velVect1(:, 2), sqrt(velVect1(:, 4).^2 + velVect1(:, 3).^2)], 'contourf');
if calcVel
[inter_vel1, plot1_vel] = plotVelocities(velVect1, '1 GPM - DT 0.005030', 'plot1');
[inter_vel2, plot2_vel] = plotVelocities(velVect2, '1 GPM - DT 0.001030', 'plot2');
[inter_vel3, plot3_vel] = plotVelocities(velVect3, '0.4 GPM - DT 0.005030', 'plot3');
end
%% SNR contour plot

if calcSNR
snr1 = plot_PIV([velVect1(:, 1:2) velVect1(:, 5)], 'contourf');
title('1 GPM - DT 0.005030 SNR')
%set(M, 'Units', 'pixels', 'Position', [10, 10, 790, 590]);
plot_foil(c_mat)
savefig('Plot1_snr')
saveas(snr1, 'Plot1_snr.jpg')
snr2 = plot_PIV([velVect2(:, 1:2) velVect2(:, 5)], 'contourf');
title('1 GPM - DT 0.001030 SNR')
plot_foil(c_mat)
savefig('Plot2_snr')
saveas(snr2, 'Plot2_snr.jpg')
snr3 = plot_PIV([velVect3(:, 1:2) velVect3(:, 5)], 'contourf');
title('0.4 GPM - DT 0.005030 SNR')
plot_foil(c_mat)
savefig('Plot3_snr')
saveas(snr3, 'Plot3_snr.jpg')

end
%% compute the mean velocity of each point

% plot_PIV_norm(velVector, x_px, y_px, x_off, y_off, x_scale_coeff, y_scale_coeff, removal_points, image, plotType)
% vect3 = plot_PIV_norm(velVect3(:, 1:4), 2, 17, 43.2, 42.2, [], strcat([DataRaw3 '\frame0000.jpg']), 'quiver');
% title('0.4 GPM - DT 0.005030 Velocity')
% savefig('plot3_velVect')
% vect2 = plot_PIV_norm(velVect2(:, 1:4), 2, 17, 43.2, 42.2, [], strcat([DataRaw2 '\frame0000.jpg']), 'quiver');
% title('1 GPM - DT 0.001030 Velocity')
% savefig('plot2_velVect')
% vect1 = plot_PIV_norm(velVect1(:, 1:4), 2, 17, 43.2, 42.2, [], strcat([DataRaw1 '\frame0000.jpg']), 'quiver');
% title('1 GPM - DT 0.005030 Velocity')
% savefig('plot1_velVect')

%% compute std of velocity of each point and compare to SNR valves

if calcSTD
%stdx1 = plot_PIV_norm([velVectSTD1(:, 1:2) velVectSTD1(:, 3)], 2, 17, 43.2, 42.2, [], strcat([DataRaw1 '\frame0000.jpg']), 'contourf');
stdx1 = plot_PIV([velVectSTD1(:, 1:2) velVectSTD1(:, 3)], 'contourf');
title('1 GPM - DT 0.005030 dx std')
plot_foil(c_mat)
savefig('Plot1_std_x')
saveas(stdx1, 'Plot1_std_x.jpg')

%stdx2 = plot_PIV_norm([velVectSTD2(:, 1:2) velVectSTD2(:, 3)], 2, 17, 43.2, 42.2, [], strcat([DataRaw2 '\frame0000.jpg']), 'contourf');
stdx2 = plot_PIV([velVectSTD2(:, 1:2) velVectSTD2(:, 3)], 'contourf');
title('1 GPM - DT 0.001030 dx std')
plot_foil(c_mat)
savefig('Plot2_std_x')
saveas(stdx2, 'Plot2_std_x.jpg')

%stdx3 = plot_PIV_norm([velVectSTD3(:, 1:2) velVectSTD3(:, 3)], 2, 17, 43.2, 42.2, [], strcat([DataRaw3 '\frame0000.jpg']), 'contourf');
stdx3 = plot_PIV([velVectSTD3(:, 1:2) velVectSTD3(:, 3)], 'contourf');
title('0.4 GPM - DT 0.005030 dx std')
plot_foil(c_mat)
savefig('Plot3_std_x')
saveas(stdx3, 'Plot3_std_x.jpg')

%stdy1 = plot_PIV_norm([velVectSTD1(:, 1:2) velVectSTD1(:, 4)], 2, 17, 43.2, 42.2, [], strcat([DataRaw1 '\frame0000.jpg']), 'contourf');
stdy1 = plot_PIV([velVectSTD1(:, 1:2) velVectSTD1(:, 4)], 'contourf');
title('1 GPM - DT 0.005030 dy std')
plot_foil(c_mat)
savefig('Plot1_std_y')
saveas(stdy1, 'Plot1_std_y.jpg')

%stdy2 = plot_PIV_norm([velVectSTD2(:, 1:2) velVectSTD2(:, 4)], 2, 17, 43.2, 42.2, [], strcat([DataRaw2 '\frame0000.jpg']), 'contourf');
stdy2 = plot_PIV([velVectSTD2(:, 1:2) velVectSTD2(:, 4)], 'contourf');
title('1 GPM - DT 0.001030 dy std')
plot_foil(c_mat)
savefig('Plot2_std_y')
saveas(stdy2, 'Plot2_std_y.jpg')

%stdy3 = plot_PIV_norm([velVectSTD3(:, 1:2) velVectSTD3(:, 4)], 2, 17, 43.2, 42.2, [], strcat([DataRaw3 '\frame0000.jpg']), 'contourf');
stdy3 = plot_PIV([velVectSTD3(:, 1:2) velVectSTD3(:, 4)], 'contourf');
title('0.4 GPM - DT 0.005030 dy std')
plot_foil(c_mat)
savefig('Plot3_std_y')
saveas(stdy3, 'Plot3_std_y.jpg')
end
%% compute advection terms

if calcAdv
plot1_adv = computeAdvection(velVect1(:, 3:4), velVect1(:, 1:2));
plotAdvection(plot1_adv, '1 GPM - DT 0.005030', 'plot1')


plot2_adv = computeAdvection(velVect2(:, 3:4), velVect2(:, 1:2));
plotAdvection(plot2_adv, '1 GPM - DT 0.001030', 'plot2')

plot3_adv = computeAdvection(velVect3(:, 3:4), velVect3(:, 1:2));
plotAdvection(plot3_adv, '0.4 GPM - DT 0.005030', 'plot3')
end

function [] = plotAdvection(advctStruct, ptitle, name)
    global c_mat

    figure
    advUX = plot_PIV([advctStruct.X advctStruct.UX], 'contourf');
    plotname = strcat([name '_UX']);
    plottitle = strcat([ptitle ' UX']);
    plot_foil(c_mat)
    title(plottitle)
    savefig(plotname)
    saveas(advUX, strcat([plotname '.jpg']))
    
    figure
    advUY = plot_PIV([advctStruct.X advctStruct.UY], 'contourf');
    plotname = strcat([name '_UY']);
    plottitle = strcat([ptitle ' UY']);
    plot_foil(c_mat)
    title(plottitle)
    savefig(plotname)
    saveas(advUY, strcat([plotname '.jpg']))
    
    figure
    advVX = plot_PIV([advctStruct.X advctStruct.VX], 'contourf');
    plotname = strcat([name '_VX']);
    plottitle = strcat([ptitle ' VX']);
    plot_foil(c_mat)
    title(plottitle)
    savefig(plotname)
    saveas(advVX, strcat([plotname '.jpg']))
    
    figure
    advVY = plot_PIV([advctStruct.X advctStruct.VY], 'contourf');
    plotname = strcat([name '_VY']);
    plottitle = strcat([ptitle ' VY']);
    plot_foil(c_mat)
    title(plottitle)
    savefig(plotname)
    saveas(advVY, strcat([plotname '.jpg']))
end

function [inter, vel] = plotVelocities(data, ptitle, name)
    global c_mat

    figure
    inter.dx = scatteredInterpolant(data(:, 1), data(:, 2), data(:, 3));
    vel.dx = plot_PIV([data(:, 1), data(:, 2), data(:, 3)], 'contourf');
    plotname = strcat([name '_vel_DX']);
    plottitle = strcat([ptitle ' DX']);
    plot_foil(c_mat)
    title(plottitle)
    savefig(plotname)
    saveas(vel.dx, strcat([plotname '.jpg']))
    
    figure
    inter.dy = scatteredInterpolant(data(:, 1), data(:, 2), data(:, 4));
    vel.dy = plot_PIV([data(:, 1), data(:, 2), data(:, 4)], 'contourf');
    plotname = strcat([name '_vel_DY']);
    plottitle = strcat([ptitle ' DY']);
    plot_foil(c_mat)
    title(plottitle)
    savefig(plotname)
    saveas(vel.dy, strcat([plotname '.jpg']))
    
    figure
    inter.avg = scatteredInterpolant(data(:, 1), data(:, 2), sqrt(data(:, 4).^2 + data(:, 3).^2));
    vel.avg = plot_PIV([data(:, 1), data(:, 2), sqrt(data(:, 4).^2 + data(:, 3).^2)], 'contourf');
    plotname = strcat([name '_vel_AVG']);
    plottitle = strcat([ptitle ' AVG']);
    plot_foil(c_mat)
    title(plottitle)
    savefig(plotname)
    saveas(vel.avg, strcat([plotname '.jpg']))
end

function [] = plot_foil(foil_mat)
    hold on
    plot(foil_mat(:, 1), foil_mat(:, 2), 'w-')
    hold off
end
