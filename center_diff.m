function [du, u] = center_diff(x, y, Uint)

    u = Uint(x, [y; y; y]);
    
    du = x(1) - x(3) / (u(1) - u(3));
end