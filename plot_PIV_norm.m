function [fig] = plot_PIV_norm(velVector, x_off, y_off, x_scale_coeff, y_scale_coeff, removal_points, image, plotType)
    

    %% normalize vector coordinates

    velVector(:, 1) =  velVector(:, 1) - min(velVector(:, 1)); 
    velVector(:, 2) =  velVector(:, 2) - min(velVector(:, 2));

    velVector(:, 1) = velVector(:, 1) / max(velVector(:, 1)); 
    velVector(:, 2) = velVector(:, 2) / max(velVector(:, 2));
    
    x_scale = x_scale_coeff * length(unique(velVector(:, 1)));
    y_scale = y_scale_coeff * length(unique(velVector(:, 2)));
    
    velVector(:, 1) = velVector(:, 1)*x_scale + x_off;
    velVector(:, 2) = velVector(:, 2)*y_scale + y_off;

%     y_px = 580;
%     x_px = 780;
%     x_scale = 43.2 * length(unique(velVector(:, 1)));
%     y_scale = 42.2 * length(unique(velVector(:, 2)));
%     x_off   = 2;
%     y_off   = 17;
    
   
    fig = figure;
    clf
    %PIV_image = imread('0001_velocity_vec_frame.jpg');
    %PIV_image = imread('frame0000.jpg');
    if image == 0
        % do not plot the image
    else
        PIV_image = imread(image);
        imshow(PIV_image);
        img_size = size(PIV_image);
    end
    

%     image_remove_x1 = [411, 415];
%     image_remove_y1 = [428, 108];
% 
%     image_remove_x2 = [456,460];
%     image_remove_y2 = [201, 291];
%    
% 
%     index_remove = find(velVector(:, 2) < image_remove_y1(2) & velVector(:, 2) < image_remove_y1(1) & velVector(:, 1) > image_remove_x1(1) & velVector(:, 1) < image_remove_x1(2));
%     index_remove = [find(velVector(:, 2) < image_remove_y2(2) & velVector(:, 2) < image_remove_y2(1) & velVector(:, 1) > image_remove_x2(1) & velVector(:, 1) < image_remove_x2(2)); index_remove];
% 
%     index_remove = sort(index_remove);
% 
%     for i = length(index_remove):-1:1
%         velVector(index_remove(i), :) = [];
%     end 

    plot_PIV(velVector, plotType);
%     hold on
%     im = imshow(PIV_image);
%     hold off
%     set(im, 'AlphaData', 0.6)
%     if strcmp(plotType, 'quiver')
%         hold on
%             quiver(velVector(:, 1), velVector(:, 2), velVector(:, 3), velVector(:, 4), 'green')
%         hold off
%     elseif strcmp(plotType, 'contour')
%         F = TriScatteredInterp(velVector(:, 1), velVector(:, 2), velVector(:, 3));
%         [qx, qy] = meshgrid(min(velVector(:, 1)):0.25:max(velVector(:, 1)), min(velVector(:, 1)):0.25:max(velVector(:, 1)));
%         qz = F(qx, qy);
%         %colormap([1 1 1; 1 1 0; 1 1 0.5])
%         hold on
%             [M,c] = contour(qx, qy, qz);
%         hold off
%         clabel(M, 'green')
%         c.LineWidth = 3;
% %         hold on 
% %             contour(velVector(:, 1), velVector(:, 2), velVector(:, 3));
% %         hold off
%     elseif strcmp(plotType, 'contourf')
%             F = TriScatteredInterp(velVector(:, 1), velVector(:, 2), velVector(:, 3));
%             [qx, qy] = meshgrid(min(velVector(:, 1)):0.25:max(velVector(:, 1)), min(velVector(:, 1)):0.25:max(velVector(:, 1)));
%             qz = F(qx, qy);
%             hold on
%                 M = contourf(qx, qy, qz);
%             hold off
%     %         hold on 
%     %             contour(velVector(:, 1), velVector(:, 2), velVector(:, 3));
%     %         hold off
%     end

end