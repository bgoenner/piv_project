
import cv2 

def blendImage(foreground, background):


    pass

fDir = './piv_figures/'
backF1 = './PIV_Data/GPM1-DT005030/raw/'
backF2 = './PIV_Data/GPM1_DT001030/raw/'
backF3 = './PIV_Data/GPM0-4_DT005030/raw/'

# Read the images
foreground = cv2.imread(fDir + "Plot1_snr.jpg")
background = cv2.imread(backF1 + "frame0000.jpg")
#alpha = cv2.imread("blend.png")
 
# Convert uint8 to float
#foreground = foreground.astype(float)
#background = background.astype(float)
 
# Normalize the alpha mask to keep intensity between 0 and 1
#alpha = alpha.astype(float)/255
 
# Multiply the foreground with the alpha matte
#foreground = cv2.multiply(alpha, foreground)
 
# Multiply the background with ( 1 - alpha )
#background = cv2.multiply(1.0 - alpha, background)
 
# Add the masked foreground and background.
#outImage = cv2.add(foreground, background)
 
# Display image

outImage = cv2.addWeighted(foreground, 0.7, background, 0.3, 0)

cv2.imshow("outImg", outImage)
cv2.waitKey(0)