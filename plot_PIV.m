function [fig, c] = plot_PIV(velVector,  plotType)
    
    fig = figure;
    h1 = axes;

    if strcmp(plotType, 'quiver')
        hold on
            quiver(velVector(:, 1), velVector(:, 2), velVector(:, 3), velVector(:, 4), 'green')
        hold off
    elseif strcmp(plotType, 'contour')
        F = TriScatteredInterp(velVector(:, 1), velVector(:, 2), velVector(:, 3));
        [qx, qy] = meshgrid(min(velVector(:, 1)):1:max(velVector(:, 1)), min(velVector(:, 2)):1:max(velVector(:, 2)));
        qz = F(qx, qy);
        hold on
            [H, c] = contour(qx, qy, qz, 'g');
        hold off
        clabel(M, 'Color', 'g', 'FontWeight', 'bold')
        c.LineWidth = 2;
        
        %axis off

    elseif strcmp(plotType, 'contourf')
            V11 = 0;
            V12 = 780;
            V21 = 0;
            V22 = 580;
            F = scatteredInterpolant(velVector(:, 1), velVector(:, 2), velVector(:, 3));
            [qx, qy] = meshgrid(min(velVector(:, 1)):1:max(velVector(:, 1)), min(velVector(:, 2)):1:max(velVector(:, 2)));
            %[qx, qy] = meshgrid(V11:1:V12, V21:1:V22);

            qz = F(qx, qy);
            
            N = max(velVector(:, 1));
            M = max(velVector(:, 2));
            s = 5;
            
            hold on
                [H, c] = contourf(qx, qy, qz);
                for k = 1:s:M
                    x = [1 N];
                    y = [k k];
                    plot(x,y,'Color','w','LineStyle','-');
                    plot(x,y,'Color','k','LineStyle',':');
                end

                for k = 1:s:N
                    x = [k k];
                    y = [1 M];
                    plot(x,y,'Color','w','LineStyle','-');
                    plot(x,y,'Color','k','LineStyle',':');
                end
            hold off
            clabel(H)
            set(h1, 'Ydir', 'reverse')
            set(h1, 'YAxisLocation', 'Right')
            %set(M, 'AlphaData', 0.5)
            
            %axis off
%             axisObj = hc.axesContour;
%             if ishandle(axisObj)
%                 handles2delete = get(axisObj,'Children');
%                 delete(handles2delete);
%                 set(axisObj,'visible','off') 
%             end
%             if (isfield(handles,'contour') && isfield(handles.contour,'hColorbar'))
%                 delete(handles.contour.hColorbar);
%                 delete(handles.contour.hColorbarLabel);
%             end
%             
            
    elseif strcmp(plotType, 'image')
            %F = TriScatteredInterp(velVector(:, 1), velVector(:, 2), velVector(:, 3));
            %[qx, qy] = meshgrid(min(velVector(:, 1)):0.25:max(velVector(:, 1)), min(velVector(:, 1)):0.25:max(velVector(:, 1)));
            %qz = F(qx, qy);
            hold on
                M = image(velVector(:, 1), velVector(:, 2), velVector(:, 3));
            hold off
            %clegend(M)
    elseif strcmp(plotType, 'gridlines')
        
        N = 780;
        M = 580;
        hold on
        for k = 1:25:M
            x = [1 N];
            y = [k k];
            plot(x,y,'Color','w','LineStyle','-');
            plot(x,y,'Color','k','LineStyle',':');
        end

        for k = 1:25:N
            x = [k k];
            y = [1 M];
            plot(x,y,'Color','w','LineStyle','-');
            plot(x,y,'Color','k','LineStyle',':');
        end
        hold off
        
        axis on
    end

end